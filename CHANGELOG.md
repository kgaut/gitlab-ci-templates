# CHANGELOG

# 0.4.13 - XX/XX/2024
- feat(template/qa): add phpunit job
- feat(template/qa/phpstan): add bodyscan 

# 0.4.12 - 23/08/2024
- feat(template/qa): remove ssh tag from job
- feat(template/qa): update rules

# 0.4.11 - 22/08/2024
- feat(documentation): fix error on preprod-assets-generation-rule documentation
- feat(metric): add job to track metrics

# 0.4.10 - 19/07/2024
- fix(assets): preprod trigger

# 0.4.9 - 16/07/2024
- feat(pre/post deploy): fix job extends 
- feat(dockerfile): add job executor build job 
- feat(deploy/pre-deploy): use custom image for job 
- fix(deploy/pre-deploy): execute only first script 

# 0.4.8 - 30/04/2024
- feat(debug): dump all vars in debug job

# 0.4.7 - 30/04/2024
- feat(assets): restore variable ASSETS_ARTEFACTS_TARGET

# 0.4.6 - 17/04/2024
- feat(config) variabalize preprod branch name cf #2

# 0.4.5 - 17/04/2024
- fix(ci): add deploy tracking
- fix(template): track deploy only if $CI_TRACKING_ENDPOINT is defined
- fix(documentation): add link to check tags

# 0.4.4 - 16/04/2024
- fix(config) update release name from preprod-CI_COMMIT_SHORT_SHA to preprod-CI_PIPELINE_IID

# 0.4.3 - 16/04/2024
- feat(script/deploy) add drush clear:cache after composer install

# 0.4.2 - 16/04/2024
- feat(config) update release name from preprod-CI_COMMIT_SHORT_SHA to preprod-CI_PIPELINE_IID

# 0.4.1 - 16/04/2024
- feat(documentation): Update docusaurus version
- feat(documentation): Update readme and intro
- fix(variable) set default value for DRUPAL_ROOT_PATH to `web` instead of `website` #7

# 0.4.0 - 10/04/2024
- feat(template/qa): Add phpstan job #4
- feat(template/qa): Add phpcs job #4
- feat(template/assets): Trigger assets jobs on preprod only when detecting changes on $CUSTOM_THEMES_FOLDER_PATH
- feat(template/scheduled-clean): allow to overide DAYS_DUMP_TO_KEEP per env #1
- fix(template/diff-repository) potential issue on specific drupal web folder #7

# 0.3.0 - 26/03/2024
- feat(refactoring) rename $DRUSH_EXEC to $DRUSH_BIN #5
- feat(refactoring) deprecate $ASSETS_ARTEFACTS_TARGET in favor of $PATH_TO_THEME #6
- fix(documentation) broken link
- feat(backup) allow to disable backup on preprod with $DISABLE_PREPROD_DUMP_ON_DEPLOY #3
- feat(deploy) add deploiement tracking job

# 0.2.2 - 20/03/2024
- fix(.pre-deploy): ssh connexion when using specific port with $SSH_USER

# 0.2.1 - 19/03/2024
- feat(doc): update readme and intro
- feat(ssh): Allow to set custom SSH_PORT
- feat(test:ssh-connexion): Use ENV_PROD_DISABLED or ENV_PREPROD_DISABLED
- feat(test:ssh-connexion): debug ~/.ssh/known_hosts

# 0.2.0 - 13/03/2024
- fix(readme): links to docs
- fix(trigger): Allow to disable an environment via ENV_PROD_DISABLED or ENV_PREPROD_DISABLED
- feat(licence): Add licence reference

# 0.1.0 - 13/03/2024
- feat(templates): add pre-deploy / post-deploy script
- fix(scripts/deploy): DRUSH_BIN: command not found

# 0.0.7 - 08/01/2024
- feat(templates): revert remove useless args

# 0.0.6 - 08/01/2024
- feat(templates): remove useless args

# 0.0.5 - 29/12/2023
- fix(diff-config): Add variable $DRUPAL_CONFIG_PATH
- fix(drupal-deploy): Set raven.settings release only if raven is enabled

# 0.0.4 - 15/12/2023
- fix(assets): deploy job

# 0.0.3 - 15/12/2023
- fix(deploy): Fix drupal tag deploy
- fix(deploy): Fix chmod error

# 0.0.2 - 15/12/2023
- fix(docusaurus): Update rule to trigger only on change when push on main

# 0.0.1 - 15/12/2023
- fix(archi): Create archi templates + doc