# CI Gitlab Templates

![Logo Gitlab CI](docs/static/img/gitlab-ci-cd.png "Logo Gitlab CI")

Templates jobs Gitlab CI, pour un déploiement facile, rapide et sécurisé 🚀

Pas grand chose ici, je vous conseille de jeter un oeil à la documentation : https://gitlab-ci-templates.kgaut.net

## Pourquoi ce projet ?

Factorisation de jobs Gitlab CI afin de mettre facilement et rapidement une pipeline en place pour un projet existant.

## TL:DR ?

Pour un drupal, voici un exemple à mettre en place en deux minutes

### .gitlab-ci.yml
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION 0.4.0 # Pour voir la dernière version : https://gitlab.com/kgaut/gitlab-ci-templates/-/tags
  PATH_TO_THEME: "./web/themes/mon_theme"
  ASSETS_IMAGE: "registry.gitlab.com/kgaut/docker-node-images:19"
  ASSETS_ARTEFACTS: "./web/themes/mon_theme/css"
  SENTRY_PROJECT : 'mon-projet'
  SENTRY_ORG : 'kgautnet'
  DB_PATH: 'files/dumps'
  DIFFS_PATH: 'files/diffs'

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/drupal/backup.yml'
      - '/templates/drupal/diff-config.yml'
      - '/templates/generic/diff-repository.yml'
      - '/templates/generic/assets.yml'
      - '/templates/drupal/deploy.yml'
      - '/templates/generic/sentry.yml'
      - '/templates/drupal/post-deploy-clear-cache.yml'
      - '/templates/generic/scheduled-clean-dump.yml'
      - '/templates/drupal/scheduled-backup.yml'
```
### Variables Gitlab CI
Il est préférable de mettre quelques variables en dehors de ce fichier, soit par ce qu'elles peuvent varier selon les environement, soit car elles doivent rester confidentielles.

Par exemple :

| Nom de la variable | Contenu                                                                                  | Exemple              | Remarque                                                           |
|--------------------|------------------------------------------------------------------------------------------|----------------------|--------------------------------------------------------------------|
| PROJECT_URL        | Url du site pour créer le dump                                                           | `monsite.com`        |                                                                    |
| PROJECT_ROOT       | Chemin relatif ou absolu vers la racine du projet                                        | `~/public_html`      |                                                                    |
| SSH_CHAIN          | Chaine de connexion SSH                                                                  | `ssh user@127.0.0.1` |                                                                    |
| SSH_HOST           | IP ou hostname du serveur                                                                | `127.0.0.1`          |                                                                    |
| SSH_USER           | Utilisateur ssh                                                                          | `user`               |                                                                    |
| SSH_PRIVATE_KEY    | Clé ssh privée pour se connecter au serveur                                              |                      | [Comment générer une clé ssh](docs/docs/extras/generer-cle-ssh.md) |
| SENTRY_AUTH_TOKEN  | Jeton d'authentification à Sentry, à définir dans les variables CI sur le project gitlab |                      |                                                                    |

[Toutes les variables](docs/docs/bases/variables.md)

### Et hop...

Un push sur une branche `preprod` lancera un déploiement en preprod et un tag de la forme `x.y.z` lancera un déploiement en prod.

## Ok, on va plus loin ?

La documentation : https://gitlab-ci-templates.kgaut.net

[Introduction](docs/docs/intro.md)

[Toutes les variables](docs/docs/bases/variables.md)
