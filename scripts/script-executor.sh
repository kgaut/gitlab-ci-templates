#!/bin/bash
set -e

PROJECT_ROOT="$1"
MIGRATION_SCRIPT_PATH="$2"
DRUSH_BIN="$3"
DRUSH_ALIAS="$4"

echo "********************"
echo "Debug"
echo "PROJECT_ROOT : $PROJECT_ROOT"
echo "DRUSH_BIN : $DRUSH_BIN"
echo "DRUSH_ALIAS : $DRUSH_ALIAS"
echo "MIGRATION_SCRIPT_PATH : $MIGRATION_SCRIPT_PATH"
echo "********************"

cd $PROJECT_ROOT
ls ${MIGRATION_SCRIPT_PATH} 2>/dev/null | while read T_SCRIPT; do
  echo "---------------------"
  echo $T_SCRIPT
  echo "Execution script $MIGRATION_SCRIPT_PATH/$T_SCRIPT"
  $DRUSH_BIN $DRUSH_ALIAS php:scr $MIGRATION_SCRIPT_PATH/$T_SCRIPT
  echo "FIN - Execution script $MIGRATION_SCRIPT_PATH/$T_SCRIPT"
done