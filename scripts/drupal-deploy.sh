#!/bin/bash
set -e

PROJECT_ROOT="$1"
DRUPAL_SITE_PATH="$2"
DRUSH_BIN="$3"
CI_ENVIRONMENT_NAME="$4"
CI_PIPELINE_IID="$5"
DRUSH_ALIAS="$7"
CI_COMMIT_REF_NAME="$6"

echo "###############################################"
echo "PROJECT_ROOT : $PROJECT_ROOT"
echo "DRUPAL_SITE_PATH : $DRUPAL_SITE_PATH"
echo "DRUSH_BIN : $DRUSH_BIN"
echo "CI_ENVIRONMENT_NAME : $CI_ENVIRONMENT_NAME"
echo "CI_PIPELINE_IID : $CI_PIPELINE_IID"
echo "DRUSH_ALIAS : $DRUSH_ALIAS"
echo "CI_COMMIT_REF_NAME : $CI_COMMIT_REF_NAME"

echo "###############################################"
echo "cd $PROJECT_ROOT"
cd "$PROJECT_ROOT"
echo "Directory : "
pwd

echo "###############################################"
echo "Changement permissions dossiers sites/default/settings"
echo "chmod +w $PROJECT_ROOT/$DRUPAL_SITE_PATH"
chmod +w "$PROJECT_ROOT/$DRUPAL_SITE_PATH"
echo "chmod +w $PROJECT_ROOT/$DRUPAL_SITE_PATH/settings.php"
chmod +w "$PROJECT_ROOT/$DRUPAL_SITE_PATH/settings.php"


if [ $CI_ENVIRONMENT_NAME = "prod" ]
then
  TAG="$6"
  git fetch --tags
  git checkout "$TAG"
else
  git fetch --all
  git reset --hard origin/$CI_COMMIT_REF_NAME
fi

echo "###############################################"
echo "composer install --no-dev"
composer install --no-dev

echo "###############################################"
echo "$DRUSH_BIN $PROJECT_DRUSH_ALIAS cr"
$DRUSH_BIN $PROJECT_DRUSH_ALIAS cr

echo "###############################################"
echo "Define environment_indicator and sentry release"
if [ $CI_ENVIRONMENT_NAME = "prod" ]
then
  $DRUSH_BIN $PROJECT_DRUSH_ALIAS sset environment_indicator.current_release "$TAG"
  if $DRUSH_BIN $PROJECT_DRUSH_ALIAS pm-list | grep 'raven' | grep -q 'Enabled'
  then
    $DRUSH_BIN $PROJECT_DRUSH_ALIAS config:set raven.settings release "$TAG" -y
  fi
else
  $DRUSH_BIN $PROJECT_DRUSH_ALIAS sset environment_indicator.current_release "$CI_COMMIT_REF_NAME-$CI_PIPELINE_IID"
  if $DRUSH_BIN $PROJECT_DRUSH_ALIAS pm-list | grep 'raven' | grep -q 'Enabled'
  then
    $DRUSH_BIN $PROJECT_DRUSH_ALIAS config:set raven.settings release "$CI_COMMIT_REF_NAME-$CI_PIPELINE_IID" -y
  fi
fi

echo "###############################################"
echo "$DRUSH_BIN $PROJECT_DRUSH_ALIAS deploy"
$DRUSH_BIN $PROJECT_DRUSH_ALIAS deploy

echo "###############################################"
echo "Restauration permissions dossiers sites/default/settings"
echo "chmod ugo-w $PROJECT_ROOT/$DRUPAL_SITE_PATH"
chmod ugo-w "$PROJECT_ROOT/$DRUPAL_SITE_PATH"
echo "chmod ugo-w $PROJECT_ROOT/$DRUPAL_SITE_PATH/settings.php"
chmod ugo-w "$PROJECT_ROOT/$DRUPAL_SITE_PATH/settings.php"
