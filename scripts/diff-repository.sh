#!/bin/bash
set -e

DRUPAL_ROOT_PATH="$1"
PROJECT_ROOT="$2"
DIFFS_PATH="$3"
CI_ENVIRONMENT_NAME="$4"

echo "********************"
echo "Debug"
echo "DRUPAL_ROOT_PATH : $DRUPAL_ROOT_PATH"
echo "PROJECT_ROOT : $PROJECT_ROOT"
echo "DIFFS_PATH : $DIFFS_PATH"
echo "CI_ENVIRONMENT_NAME : $CI_ENVIRONMENT_NAME"
echo "********************"

cd "$PROJECT_ROOT"

if [[ $(git diff "$DRUPAL_ROOT_PATH") ]]; then
    FILENAME="$(date +"%Y-%m-%d_%H-%M-%S")-diff.patch"
    git diff "$DRUPAL_ROOT_PATH" > "./$DIFFS_PATH/$FILENAME"
    #git checkout ./
    echo "---------------------------------";
    echo "modifications en $CI_ENVIRONMENT_NAME detectées, patch créé : $FILENAME";
    echo "Liste des fichiers modifiés : ";
    echo $(git diff --name-only)
    echo "---------------------------------";
    git checkout ./
    FAIL;
else
    if [[ $(git ls-files --others --exclude-standard) ]]; then
        echo "---------------------------------";
        echo "Fichiers non versionnés : "
        echo $(git ls-files --others --exclude-standard)
        echo "---------------------------------";
        FAIL;
    else
        echo "Pas de modification en $CI_ENVIRONMENT_NAME à signaler"
    fi
fi
