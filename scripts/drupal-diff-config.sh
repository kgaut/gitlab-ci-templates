#!/bin/bash
set -e

PROJECT_ROOT="$1"
DIFFS_PATH="$2"
CI_ENVIRONMENT_NAME="$3"
DRUPAL_CONFIG_PATH="$4"
DRUSH_BIN="$5"
PROJECT_DRUSH_ALIAS="$6"

echo "********************"
echo "Debug"
echo "PROJECT_ROOT : $PROJECT_ROOT"
echo "DIFFS_PATH : $DIFFS_PATH"
echo "CI_ENVIRONMENT_NAME : $CI_ENVIRONMENT_NAME"
echo "DRUPAL_CONFIG_PATH : $DRUPAL_CONFIG_PATH"
echo "DRUSH_BIN : $DRUSH_BIN"
echo "PROJECT_DRUSH_ALIAS : $PROJECT_DRUSH_ALIAS"
echo "********************"

cd "$PROJECT_ROOT"

$DRUSH_BIN $PROJECT_DRUSH_ALIAS cr
$DRUSH_BIN $PROJECT_DRUSH_ALIAS cex -y
if [[ $(git diff $DRUPAL_CONFIG_PATH) ]]; then
    echo "---------------------------------";
    FILENAME="$(date +"%Y-%m-%d_%H-%M-%S")-config.patch"
    git diff $DRUPAL_CONFIG_PATH > "./$DIFFS_PATH/$FILENAME"
    echo "Modifications de configuration en $CI_ENVIRONMENT_NAME detectées, patch créé : $FILENAME";
    git checkout ./
    echo "---------------------------------";
fi

cd "$PROJECT_ROOT/$DRUPAL_CONFIG_PATH"
if [[ $(git ls-files --others --exclude-standard) ]]; then
    echo "---------------------------------";
    FILENAME="$(date +"%Y-%m-%d_%H-%M-%S")-config.tar"
    echo "Config en prod non versionnée : "
    echo $(git ls-files --others --exclude-standard)
    tar -zcvf "../../diffs/$FILENAME" ./
    echo "Archive crée : $FILENAME";
    rm ./*/*.yml
    git checkout ./
    echo "---------------------------------";
fi
