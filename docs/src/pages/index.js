import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';

import Heading from '@theme/Heading';
import styles from './index.module.css';
import gitlabImg from '@site/static/img/gitlab-ci-cd.png';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <Heading as="h1" className="hero__title">
          {siteConfig.title}
        </Heading>
          <img src={gitlabImg} style={{height:'150px'}}/>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
            Lire la doc
          </Link>
            <br />
        </div>
          <br />
          <p>
              <Link
                  className=""
                  style={{color:"white"}}
                  to="https://gitlab.com/kgaut/gitlab-ci-templates">
                  Non, je veux juste voir les sources du projet
              </Link></p>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Gitlab CI Templates - Documentation`}
      description="Templates jobs Gitlab CI, pour un déploiement facile, rapide et sécurisé 🚀">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
