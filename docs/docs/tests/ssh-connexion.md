---
sidebar_position: 1
---

# SSH Connexion

Job permettant de tester si la connexion SSH fonctionne.

## Utilisation

```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/debug/ssh-connexion.yml'
```

## Variables nécessaires

| Nom de la variable | Contenu                                     | Exemple            | Remarque                                                 |
|--------------------|---------------------------------------------|--------------------|----------------------------------------------------------|
| SSH_CHAIN          | Chaine de connexion SSH                     | `ssh user@serveur` |                                                          |
| SSH_PRIVATE_KEY    | Clé ssh privée pour se connecter au serveur |                    | [Comment générer une clé ssh](../extras/generer-cle-ssh) |
| SSH_HOST           | IP ou hostname du serveur                   | `127.0.0.1`        |                                                          |

## Liste des jobs

### prod:test:ssh-connexion
execute un `whoami`, `pwd` et `hostname` sur le serveur distant de prod

### preprod:test:ssh-connexion
execute un `whoami`, `pwd` et `hostname` sur le serveur distant de preprod