---
sidebar_position: 1
---

# Hello World

Job permettant de tester si ça marche :)

## Utilisation

```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/debug/hello-world.yml'
```

## Liste des jobs

### hello-world
Affiche « hello world » dans un job.