---
sidebar_position: 2
---

# Docusaurus

Permet de générer un site static pour la documentation de son projet


## Installation

À la racine de votre projet, créez un dossier `docs`

Ajouter un container à votre docker-compose.yml

```yaml
  nodedoc:
    image: registry.gitlab.com/kgaut/docker-node-images:19
    volumes:
      - ./docs:/app
    ports:
      - '3000:3000'
    tty: true
```

Connexion au container 
```bash
docker compose exec nodedoc sh
```
Installation de docusaurus
```bash
npx create-docusaurus@latest ./sources classic
cd sources/
mv * ../
mv .gitignore ../
cd ../
rm -Rf sources
```

Et c'est parti !

## Lancement du serveur local

```
npx docusaurus start --host 0.0.0.0
```
Le site sera accessible sur [localhost:3000](localhost:3000)
