---
sidebar_position: 3
---

# Pipeline scheduled
Mécanisme interne à Gitlab qui permet de lancer une pipeline à un interval défini (par exemple toute les nuits).

Pour configurer, sur votre projet gitlab : Build / Pipeline schedules / Create a new pipeline schedule

![img alt](./img/scheduled-pipeline.png)

Pour lancer un job scheduled, il lui faudra mettre la rule suivante :

```yaml
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
```