---
sidebar_position: 1
---

# Générer une clé SSH


## Génération de la clé
```bash
ssh-keygen -t ed25519 -C "monprojet@gitlab.com" -f ~/tmp/cle_mon_projet
```

## Clé privée dans la variable

Le contenu du fichier `~/tmp/cle_mon_projet` sera la clé privée de la ci, la variable `SSH_PRIVATE_KEY`, à renseigner au niveau de gitlab, de préférence en protected.

## Ajout de la clé publique au serveur 

Le contenu du fichier `~/tmp/cle_mon_projet.pub` sera la clé public à ajouter à votre serveur de destination, cela peut-être fait via la commande suivante : 

```bash
ssh-copy-id -i ~/tmp/cle_mon_projet user@serveur
```

## Test de la clé
Pour tester que la clé fonctionne

```bash
ssh user@serveur -i ~/tmp/cle_mon_projet
```