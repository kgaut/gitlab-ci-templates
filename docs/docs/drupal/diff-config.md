---
sidebar_position: 2
---

# diff-config

Job permettant de vérifier que la config exportées (et versionnée) est bien identique à celle en base de données 

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/drupa/diff-config.yml'
```

## Variables nécessaires
| Nom de la variable  | Contenu                                                                              | Exemple            | Remarque                                                        |
|---------------------|--------------------------------------------------------------------------------------|--------------------|-----------------------------------------------------------------|
| DIFFS_PATH          | Dossier relatif à la racine du projet devant contenir les patchs générés par le diff | `files/diffs`      | ce dossier doit exister dans l'arboresence est être .gitignored |
| DRUSH_ALIAS         | Alias drush ou paramètre supplémentaire si nécessaire                                | `@monsite`         | ne pas le définir si inutile                                    |
| DRUSH_BIN          | Chemin relatif à la racine du projet vers le binaire drush                           | `vendor/bin/drush` |                                                                 |
| DRUPAL_CONFIG_PATH  | Chemin relatif à la racine du projet vers le dossier de config sync                  | `config/sync`      | Si par défault à `config/sync`, pas besoin de le définir        |
| PROJECT_ROOT        | Chemin relatif ou absolu vers la racine du projet                                    | `~/public_html`    |                                                                 |
| SSH_CHAIN           | Chaine de connexion SSH                                                              | `ssh user@serveur` |                                                                 |

## Liste des jobs

### prod:deploy
Lance un déploiement en prod

### preprod:deploy
Lance un déploiement en préproduction