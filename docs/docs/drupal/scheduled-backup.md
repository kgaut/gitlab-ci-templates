---
sidebar_position: 10
---


# Job planifié de backup

Permet de faire un backup de la base de données automatiquement à interval régulier.

Nécessite la configuration d'une [Scheduled pipeline](../extras/scheduled-pipeline).

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  DRUSH_BIN: 'vendor/bin/drush'

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/drupal/scheduled-backup.yml'
```

## Variables nécessaires
| Nom de la variable | Contenu                                                         | Exemple            | Remarque                                                 |
|------------------|-----------------------------------------------------------------|--------------------|----------------------------------------------------------|
| SSH_CHAIN        | Chaine de connexion SSH                                         | `ssh user@serveur` |                                                          |
| SSH_PRIVATE_KEY  | Clé ssh privée pour se connecter au serveur                     |                    | [Comment générer une clé ssh](../extras/generer-cle-ssh) |
| PROJECT_ROOT  | Chemin relatif ou absolu vers la racine du projet               | `~/public_html`    |                                                          |
| DB_PATH    | Dossier relatif à la racine du projet devant contenir les dumps | `files/dumps`      |                                                          |
| PROJECT_URL   | Url du site pour créer le dump                                  | `monsite.com`      |                                                          |
| DRUSH_BIN    | Chemin relatif à la racine du projet vers le binaire drush      | `vendor/bin/drush` |                                                          |
| DRUSH_ALIAS   | Alias drush ou paramètre supplémentaire si nécessaire           | `@monsite`         | ne pas le définir si inutile                             |

## Liste des jobs

### prod:scheduled-backup
Lance un backup en production

### preprod:scheduled-clean
Lance un backup en preproduction