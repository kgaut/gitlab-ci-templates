---
sidebar_position: 3
---

# Vidage de cache post déploiement

Un petit coup de vidage de cache après un déploiement, ça ne fait jamais de mal...

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/drupal/post-deploy-clear-cache.yml'
```

## Variables nécessaires
| Nom de la variable | Contenu                                                                              | Exemple            | Remarque                                                        |
|------------------|--------------------------------------------------------------------------------------|--------------------|-----------------------------------------------------------------|
| DRUSH_ALIAS   | Alias drush ou paramètre supplémentaire si nécessaire           | `@monsite`         | ne pas le définir si inutile                             |
| DRUSH_BIN    | Chemin relatif à la racine du projet vers le binaire drush      | `vendor/bin/drush` |                                                          |
| PROJECT_ROOT  | Chemin relatif ou absolu vers la racine du projet               | `~/public_html`    |                                                          |
| SSH_CHAIN        | Chaine de connexion SSH                                                              | `ssh user@serveur` |                                                                 |

## Liste des jobs

### prod:clear-cache
Vide la cache de la prod après un déploiement

### preprod:clear-cache
Vide la cache de la preprod après un déploiement