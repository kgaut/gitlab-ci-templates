---
sidebar_position: 4
---

# Déploiement

Job permettant déployer la base code sur le serveur.

Lance un `composer install --no-dev`, un `drush deploy`...

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  DRUPAL_SITE_PATH : 'web/sites/default'

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/drupa/deploy.yml'
```


## Scripts pre-déploiement et post-déploiement
Il est possible d'executer des script drush avant le déploiement et après le déploiement

### Scripts pré-déploiement
Lancés avant le job deploy, avant donc que le dépôt ne soit mis à jour sur le serveur, que le composer install soit lancé... 

Il est utile pour désactiver un module avant sa suppression effective du composer.json

### Scripts post-déploiement
Lancés une fois que tout est déployé et que les updates de base de données sont joués.

### Emplacements des scripts

Par défault: 
 - les scripts de pré-déploiement doivent être dans le dossier `scripts/pre-deploy`
 - les scripts de post-déploiement doivent être dans le dossier `scripts/post-deploy`

Tout cela peut-être écrasé via les variables 
 - `$MIGRATION_SCRIPT_PATH` défault à `scripts`
 - `$MIGRATION_SCRIPT_PREDEPLOY_PATH` défault à `$MIGRATION_SCRIPT_PATH/pre-deploy`
 - `MIGRATION_SCRIPT_POSTDEPLOY_PATH` défault à `$MIGRATION_SCRIPT_PATH/post-deploy`

### Notes
- Si ces dossiers sont vide, alors rien n'est executé.
- Si vous ne supprimez pas un script de migration, il sera executé à chaque déploiement, à vous de gérer le cas si un script ne doit pas être lancé une seconde fois.

## Variables nécessaires
| Nom de la variable               | Contenu                                                                                  | Exemple              | Remarque                                                           |
|----------------------------------|------------------------------------------------------------------------------------------|----------------------|--------------------------------------------------------------------|
| DIFFS_PATH                       | Dossier relatif à la racine du projet devant contenir les patchs générés par le diff     | `files/diffs`       | ce dossier doit exister dans l'arboresence est être .gitignored    |
| DRUSH_ALIAS                      | Alias drush ou paramètre supplémentaire si nécessaire                                    | `@monsite`          | ne pas le définir si inutile                                       |
| DRUSH_BIN                       | Chemin relatif à la racine du projet vers le binaire drush                               | `vendor/bin/drush`  |                                                                    |
| PROJECT_ROOT                     | Chemin relatif ou absolu vers la racine du projet                                        | `~/public_html`     |                                                                    |
| SSH_CHAIN                        | Chaine de connexion SSH                                                                  | `ssh user@serveur`  |                                                                    |
| DRUPAL_SITE_PATH                 | Chemin vers le site drupal depuis le docroot                                             | `web/sites/default` | variable définie par défaut à `web/sites/default`                  |
| MIGRATION_SCRIPT_PATH            | Chemin vers le dossier contenant les dossiers de scripts de migration depuis le docroot  | `scripts` | variable définie par défaut à `scripts`                            |
| MIGRATION_SCRIPT_PREDEPLOY_PATH  | Chemin vers le site drupal depuis le docroot                                             | `$MIGRATION_SCRIPT_PATH/pre-deploy` | variable définie par défaut à `$MIGRATION_SCRIPT_PATH/pre-deploy`  |
| MIGRATION_SCRIPT_POSTDEPLOY_PATH | Chemin vers le site drupal depuis le docroot                                             | `$MIGRATION_SCRIPT_PATH/post-deploy` | variable définie par défaut à `$MIGRATION_SCRIPT_PATH/post-deploy` |

## Liste des jobs

### pre-deploy
Lance les scripts de pré-déploiement 

### deploy
Lance le déploiement
 - mise à jour du dépôt
 - composer install
 - drush deploy...

### post-deploy
Lance les scripts de post-déploiement
