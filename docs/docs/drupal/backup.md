---
sidebar_position: 1
---

# Backup

Jobs permettant de faire des backups de la base de données via drush.

## Variables nécessaires
| Nom de la variable              | Contenu                                                         | Exemple            | Remarque                                                  |
|---------------------------------|-----------------------------------------------------------------|--------------------|-----------------------------------------------------------|
| SSH_CHAIN                       | Chaine de connexion SSH                                         | `ssh user@serveur` |                                                           |
| SSH_PRIVATE_KEY                 | Clé ssh privée pour se connecter au serveur                     |                    | [Comment générer une clé ssh](../extras/generer-cle-ssh)  |
| PROJECT_ROOT                    | Chemin relatif ou absolu vers la racine du projet               | `~/public_html`    |                                                           |
| DB_PATH                         | Dossier relatif à la racine du projet devant contenir les dumps | `files/dumps`      |                                                           |
| PROJECT_URL                     | Url du site pour créer le dump                                  | `monsite.com`      |                                                           |
| DRUSH_BIN                       | Chemin relatif à la racine du projet vers le binaire drush      | `vendor/bin/drush` |                                                           |
| DRUSH_ALIAS                     | Alias drush ou paramètre supplémentaire si nécessaire           | `@monsite`         | ne pas le définir si inutile                              |
| DISABLE_PREPROD_DUMP_ON_DEPLOY  | Désactivation des dump lors du déploiement en preprod           | `1`                | par défault à 0                                           |
