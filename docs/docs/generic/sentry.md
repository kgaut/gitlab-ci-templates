---
sidebar_position: 3
---


# Sentry

Job permettant d'intégrer votre projet sur sentry et de créer les release

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  SENTRY_PROJECT : 'clearblue-tools'
  SENTRY_ORG : 'kgautnet'

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/generic/sentry.yml'
```

## Variables nécessaires
| Nom de la variable | Contenu                                                                                             | Exemple    | Remarque                                                        |
|------------------|-----------------------------------------------------------------------------------------------------|------------|-----------------------------------------------------------------|
| SENTRY_ORG | Slug de l'organisation sentry, pour https://kgautnet.sentry.io, renseignez `kgautnet` | `kgautnet` | ce dossier doit exister dans l'arboresence est être .gitignored |
| SENTRY_PROJECT | Slug du projet sentry, pour https://kgautnet.sentry.io/projects/mon-projet/, renseignez `mon-projet` | `mon-projet` | |
| SENTRY_AUTH_TOKEN | Jeton d'authentification à Sentry, à définir dans les variables CI sur le project gitlab  | | |

## Liste des jobs

### prod:sentry-release
Crée la release de prod en post déploiement sur Sentry

### preprod:sentry-release
Crée la release de preprod en post déploiement sur Sentry