---
sidebar_position: 3
---

# diff-repository

Job permettant de valider l'intégrité du serveur git de destination avant de lancer le déploiement.

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/generic/diff-repository.yml'
```

## Variables nécessaires
| Nom de la variable | Contenu                                                                               | Exemple            | Remarque                                                        |
|------------------|---------------------------------------------------------------------------------------|--------------------|-----------------------------------------------------------------|
| DIFFS_PATH        | CDossier relatif à la racine du projet devant contenir les patchs générés par le diff | `files/diffs`      | ce dossier doit exister dans l'arboresence est être .gitignored |
| PROJECT_ROOT  | Chemin relatif ou absolu vers la racine du projet               | `~/public_html`    |                                                          |
| SSH_CHAIN        | Chaine de connexion SSH                                                               | `ssh user@serveur` |                                                                 |

## Liste des jobs

### prod:diff:repository
Valide l'intégrité du dépôt déployé en production

### preprod:diff:repository
Valide l'intégrité du dépôt déployé en préproduction 