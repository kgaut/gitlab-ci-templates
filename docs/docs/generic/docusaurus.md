---
sidebar_position: 5
---

# Docusaurus

Job permettant de compiler et publier sur gitlab pages une doc docusaurus.

Ce job ne sera lancé que lors d'un changement dans le dossier $DOCUSAURUS_FOLDER lors d'un push sur la branche `main`.

[Inclusion de docusaurus dans un projet](../extras/docusaurus)

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  DOCUSAURUS_FOLDER: 'docs'

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/docusaurus.yml'
```

## Variables nécessaires
| Nom de la variable | Contenu                                                         | Exemple          | Remarque                                                 |
|------------------|-----------------------------------------------------------------|------------------|----------------------------------------------------------|
| DOCUSAURUS_FOLDER        | Dossier contanant la documentation                              | `docs` | par défault à `docs`                                           |

## Liste des jobs

### pages
Compilation de la documentation docusaurus, sera automatiquement suivi d'un job gitlab pages si activé sur l'instance. 