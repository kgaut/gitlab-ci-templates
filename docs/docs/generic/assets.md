---
sidebar_position: 3
---

# Construction des assets front

Il est peut probable que vous puissiez utiliser ces jobs tel quels mais, vous pouvez vous en servir comme exemple à mettre dans votre `.gitlab-ci.yml`.

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  PATH_TO_THEME: "./web/themes/wam_theme" # dossier contenant le gulp file / package.json...
  ASSETS_IMAGE: "registry.gitlab.com/kgaut/docker-node-images:19"
  ASSETS_ARTEFACTS: "./website/themes/wam_theme/css"
  ASSETS_ARTEFACTS_TARGET: "./website/themes/wam_theme"

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/generic/assets.yml'
```
## Variables nécessaires
| Nom de la variable        | Contenu                                                                     | Exemple                      | Remarque                        |
|---------------------------|-----------------------------------------------------------------------------|------------------------------|---------------------------------|
| PATH_TO_THEME             | Le chemin vers le dossier contenant le package.json                         | `./web/themes/wam_theme`     | si à la racine, renseignez `./` |
| ASSETS_IMAGE              | Le tag de l'image à utiliser pour la compilation                            | `node:19`                    |                                 |
| ASSETS_ARTEFACTS          | Le dossier contenant le resultat du build (qui sera envoyé sur le serveur ) | `./web/themes/wam_theme/css` |                                 |
| ASSETS_ARTEFACTS_TARGET   | Le dossier cible devant contenir le résultat du build                       | `./web/themes/wam_theme`     |                                 |


## Liste des jobs

### prod:assets-generation
Compilation des assets pour la prod

### preprod:assets-generation
Compilation des assets pour la preprod, uniquement lorsqu'il y a un changement dans le dossier 'web/themes/custom'

** Note :** si `web/themes/custom` n'est pas correcte pour votre infra, vous devrez alors surchager le job `preprod-assets-generation-rule` suivant dans votre .gitlab-ci.yml en changeant la dernière ligne : 
```yaml
.preprod-assets-generation-rule:
  rules:
    - if: $ENV_PREPROD_DISABLED != "1" && $CI_COMMIT_BRANCH == $PREPROD_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
      changes:
        - "web/themes/custom/**/*"
```

### prod:assets-deploy
Rsync des assets vers la prod

### preprod:assets-deploy
Rsync des assets vers la preprod, uniquement lorsqu'il y a un changement dans le dossier $CUSTOM_THEMES_FOLDER_PATH (par défaut : 'web/themes/custom').

## Surcharge

Il est probable que vous ne puissiez utiliser ces jobs directement, voici ce que vous pouvez copier / coller dans votre `.gitlab-ci.yml` pour l'adapter à vos besoin : 

```yaml
variables:
  PATH_TO_THEME: "./website/themes/cb_tools"
  ASSETS_IMAGE: "registry.gitlab.com/kgaut/docker-node-images:19"
  ASSETS_ARTEFACTS: "./website/themes/cb_tools/css"

.assets-generation:
  extends:
    - .job-docker
  image: $ASSETS_IMAGE
  stage : deploy
  script:
    - cd $PATH_TO_THEME
    - npm ci
    - npm run sass-compile
  artifacts:
    paths:
      - $ASSETS_ARTEFACTS
    expire_in: 20 minutes

prod:assets-generation:
  extends:
    - .prod
    - .assets-generation

preprod:assets-generation:
  extends:
    - .preprod
    - .assets-generation

.assets-deploy:
  extends:
    - .ssh
  stage : deploy
  script:
    - rsync -avhzi --delete --stats $ASSETS_ARTEFACTS -e ssh $SSH_USER@$SSH_HOST:$PROJECT_ROOT/$PATH_TO_THEME
    - $SSH_CHAIN "$DRUSH_BIN $DRUSH_ALIAS cr"

prod:assets-deploy:
  extends:
    - .prod
    - .assets-deploy
  needs:
    - prod:assets-generation

preprod:assets-deploy:
  extends:
    - .preprod
    - .assets-deploy
  needs:
    - preprod:assets-generation
```