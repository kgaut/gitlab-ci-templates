---
sidebar_position: 4
---


# Job _scheduled_ nettoyage des vieux dumps

Permet de faire le ménage dans les dumps plus vieux que X jours.

Nécessite la configuration d'une [Scheduled pipeline](../extras/scheduled-pipeline).

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  DAYS_DUMP_TO_KEEP: 7
  DAYS_DUMP_TO_KEEP_PROD: 7
  DAYS_DUMP_TO_KEEP_PREPROD: 3

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/generic/stages-variables-extends.yml'
      - '/templates/generic/scheduled-clean-dump.yml'
```

## Variables nécessaires
| Nom de la variable        | Contenu                                                          | Exemple            | Remarque                                                  |
|---------------------------|------------------------------------------------------------------|--------------------|-----------------------------------------------------------|
| DAYS_DUMP_TO_KEEP         | Nombre de jours de dump à garder                                 | `7`                | Par défaut à 7                                            |
| SSH_CHAIN                 | Chaine de connexion SSH                                          | `ssh user@serveur` |                                                           |
| SSH_PRIVATE_KEY           | Clé ssh privée pour se connecter au serveur                      |                    | [Comment générer une clé ssh](../extras/generer-cle-ssh)  |
| PROJECT_ROOT              | Chemin relatif ou absolu vers la racine du projet                | `~/public_html`    |                                                           |
| DB_PATH                   | Dossier relatif à la racine du projet devant contenir les dumps  | `files/dumps`      |                                                           |
| DAYS_DUMP_TO_KEEP_PROD    | Nombre de jours de dump à garder en prod                         | 7                  | Par défaut à $DAYS_DUMP_TO_KEEP                           |
| DAYS_DUMP_TO_KEEP_PREPROD | Nombre de jours de dump à garder en preprod                      | 3                  | Par défaut à $DAYS_DUMP_TO_KEEP                           |

## Liste des jobs

### prod:scheduled-clean
Fait le ménage dans les dumps en production

### preprod:scheduled-clean
Fait le ménage dans les dumps en preproduction