---
sidebar_position: 1
---

# PHPCS

Intégration de phpcs, création d'un rapport en artefact

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  PHPCS_DOCKER_IMAGE: wodby/drupal-php:7.4-dev
  PHPCS_QA_FILE: files/phpcs.txt
  PHPCS_CONFIG_FILE: phpcs.xml
  PHPCS_ALLOW_FAILURE: "true"


include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/qa/phpcs.yml'
```
## Variables nécessaires
| Nom de la variable    | Contenu                                                                    | Exemple                    | Remarque |
|-----------------------|----------------------------------------------------------------------------|----------------------------|----------|
| PHPCS_DOCKER_IMAGE  | Image docker à utiliser pour executer le test                              | `wodby/drupal-php:7.4-dev` |          |
| PHPCS_QA_FILE       | Chemin et nom du fichier de sorti qui sera en artefact                     | `files/phpcs.txt`          |          |
| PHPCS_CONFIG_FILE   | Fichier de config phpcs                                                    | `phpcs.xml`                |          |
| PHPCS_ALLOW_FAILURE | Si à une autre valeur que "true", alors le job plantera et arrêtera la CI. | `"true"`                   |          |

