---
sidebar_position: 1
---

# PHPStan

Intégration de phpstan, création d'un rapport en artefact

## Utilisation
```yaml
variables:
  CI_TEMPLATE_VERSION: &CI_TEMPLATE_VERSION main
  PHPSTAN_DOCKER_IMAGE: wodby/drupal-php:7.4-dev
  PHPSTAN_QA_FILE: files/phpstan.txt
  PHPSTAN_CONFIG_FILE: phpstan.neon
  PHPSTAN_ALLOW_FAILURE: "true"

include:
  - project: kgaut/gitlab-ci-templates
    ref: *CI_TEMPLATE_VERSION
    file:
      - '/templates/qa/phpstan.yml'
```
## Variables nécessaires
| Nom de la variable    | Contenu                                                                     | Exemple                     | Remarque |
|-----------------------|-----------------------------------------------------------------------------|-----------------------------|----------|
| PHPSTAN_DOCKER_IMAGE  | Image docker à utiliser pour executer le test                               | `wodby/drupal-php:7.4-dev`  |          |
| PHPSTAN_QA_FILE       | Chemin et nom du fichier de sorti qui sera en artefact                      | `files/phpstan.txt`         |          |
| PHPSTAN_CONFIG_FILE   | Fichier de config phpstan                                                   | `phpstan.neon`              |          |
| PHPSTAN_ALLOW_FAILURE | Si à une autre valeur que "true", alors le job plantera et arrêtera la CI.  | `"true"`                    |          |

