---
sidebar_position: 10
---

# Toutes les variables

| Nom de la variable               | Contenu                                                                                              | Exemple                              | Remarque                                                           |
|----------------------------------|------------------------------------------------------------------------------------------------------|--------------------------------------|--------------------------------------------------------------------|
| ENV_PROD_DISABLED                | Désactiver l'environnement de prod                                                                   | `1`                                  | par défault à `0`                                                  |
| ENV_PREPROD_DISABLED             | Désactiver l'environnement de preprod                                                                | `1`                                  | par défault à `0`                                                  |
| DB_PATH                          | Dossier relatif à la racine du projet devant contenir les dumps                                      | `files/dumps`                        |                                                                    |
| DOCUSAURUS_FOLDER                | Dossier contanant la documentation                                                                   | `docs`                               | par défault à `docs`                                               |
| DRUSH_ALIAS                      | Alias drush ou paramètre supplémentaire si nécessaire                                                | `@monsite`                           | ne pas le définir si inutile                                       |
| DRUSH_BIN                        | Chemin relatif à la racine du projet vers le binaire drush                                           | `vendor/bin/drush`                   |                                                                    |
| PROJECT_URL                      | Url du site pour créer le dump                                                                       | `monsite.com`                        |                                                                    |
| PROJECT_ROOT                     | Chemin relatif ou absolu vers la racine du projet                                                    | `~/public_html`                      |                                                                    |
| SSH_CHAIN                        | Chaine de connexion SSH                                                                              | `ssh user@127.0.0.1`                 |                                                                    |
| SSH_PORT                         | Port SSH du serveur si différent de 22                                                               | `2222`                               | par défault à 22                                                   |
| SSH_USER                         | Utilisateur ssh                                                                                      | `user`                               |                                                                    |
| SSH_HOST                         | IP ou hostname du serveur                                                                            | `127.0.0.1`                          |                                                                    |
| SSH_PRIVATE_KEY                  | Clé ssh privée pour se connecter au serveur                                                          |                                      | [Comment générer une clé ssh](../extras/generer-cle-ssh)           |
| SENTRY_ORG                       | Slug de l'organisation sentry, pour https://kgautnet.sentry.io, renseignez `kgautnet`                | `kgautnet`                           | ce dossier doit exister dans l'arboresence est être .gitignored    |
| SENTRY_PROJECT                   | Slug du projet sentry, pour https://kgautnet.sentry.io/projects/mon-projet/, renseignez `mon-projet` | `mon-projet`                         |                                                                    |
| SENTRY_AUTH_TOKEN                | Jeton d'authentification à Sentry, à définir dans les variables CI sur le project gitlab             |                                      |                                                                    |
| DRUPAL_CONFIG_PATH               | Chemin relatif à la racine du projet vers le dossier de config sync                                  | `config/sync`                        | Si par défault à `config/sync`, pas besoin de le définir           |
| MIGRATION_SCRIPT_PATH            | Chemin vers le dossier contenant les dossiers de scripts de migration depuis le docroot              | `scripts`                            | variable définie par défaut à `scripts`                            |
| MIGRATION_SCRIPT_PREDEPLOY_PATH  | Chemin vers le site drupal depuis le docroot                                                         | `$MIGRATION_SCRIPT_PATH/pre-deploy`  | variable définie par défaut à `$MIGRATION_SCRIPT_PATH/pre-deploy`  |
| MIGRATION_SCRIPT_POSTDEPLOY_PATH | Chemin vers le site drupal depuis le docroot                                                         | `$MIGRATION_SCRIPT_PATH/post-deploy` | variable définie par défaut à `$MIGRATION_SCRIPT_PATH/post-deploy` |
| DAYS_DUMP_TO_KEEP                | Nombre de jours de dump à garder                                                                     | `7`                                  | Par défaut à 7                                                     |
| DAYS_DUMP_TO_KEEP_PROD           | Nombre de jours de dump à garder en prod                                                             | `7`                                  | Par défaut à $DAYS_DUMP_TO_KEEP                                    |
| DAYS_DUMP_TO_KEEP_PREPROD        | Nombre de jours de dump à garder en preprod                                                          | `3`                                  | Par défaut à $DAYS_DUMP_TO_KEEP                                    |
| DRUPAL_SITE_PATH                 | Chemin vers le site drupal depuis le docroot                                                         | `web/sites/default`                  | variable définie par défaut à `web/sites/default`                  |
| DRUPAL_ROOT_PATH                 | Chemin vers le root de drupal                                                                        | `web`                                | variable définie par défaut à `web`                                |
| PREPROD_BRANCH                   | Nom de la branche pour l'environement de preprod                                                     | `preprod`                            | variable définie par défaut à `preprod`                            |

