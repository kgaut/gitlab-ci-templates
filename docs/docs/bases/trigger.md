---
sidebar_position: 3
---

# Les déclencheurs

Le fichier `stages-variables-extends.yml` contient deux extends qui contiennent les règles de déclenchement des jobs : 

```yaml
.prod:
  rules:
    - if: '$CI_COMMIT_TAG =~ /^\d+.\d+.\d+/'
  environment:
    name: prod

.preprod:
  rules:
    - if: $CI_COMMIT_BRANCH == "preprod" && $CI_PIPELINE_SOURCE != "schedule"
  environment:
    name: preprod
```

Ainsi les jobs qui étendent `.prod` ne se déclencheront que dans le cas d'un tag de le forme x.y.z (x, y et z étant des nombres entiers)

Les jobs qui étendent .preprod ne se lanceront que lors d'un push sur la branche `preprod`.