---
sidebar_position: 1
---
# Principes de base

## Environements
2 environements de déploiement :
- prod
- preprod

Le déploiement en prod se fait lors de tag `x.y.z`, le déploiement en preprod se fait lors d'un push sur la branche `preprod`

[Plus d'information sur les déclencheurs](../bases/trigger)


## Variables

Pour le bon fonctionnement des jobs, des variables sont nécessaires, certaines peuvent être spécifiques à l'environement (`prod` ou `preprod`).

Il est recommandé de passer en `protected` les variables critiques (clé ssh privée par exemple), cela impliquera que la branche `preprod` soit elle aussi `protected` ainsi que les tags `*.*.*`. cela se configure dans gitlab: Settings / Repository / Protected tags & Protected branches.

[Plus d'information sur les variables](../bases/variables)