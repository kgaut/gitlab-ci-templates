---
sidebar_position: 2
---

# Les stages

Un paquet de _stages_ sont proposés: 

```yaml
stages:
  - QA
  - backup
  - predeploy
  - deploy
  - postdeploy
  - scheduled

```