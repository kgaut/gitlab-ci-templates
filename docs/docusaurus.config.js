// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'CI Gitlab Templates',
  tagline: 'Templates de déploiement Gitlab CI',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://gitlab-ci-templates.kgaut.net',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'kgaut', // Usually your GitHub org/user name.
  projectName: 'gitlab-ci-templates', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  plugins: [
    'docusaurus-plugin-matomo',
  ],
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
              'https://gitlab.com/-/ide/project/kgaut/gitlab-ci-templates/edit/main/-/docs/',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/gitlab-ci-cd.png',
      navbar: {
        title: 'Gitlab CI Templates',
        logo: {
          alt: 'My Site Logo',
          src: 'img/gitlab-ci-cd.png',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Documentation',
          },
          {
            href: 'https://gitlab.com/kgaut/gitlab-ci-templates',
            label: 'Gitlab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Liens',
            items: [
              {
                label: 'Documentation',
                to: '/docs/intro',
              },
              {
                label: 'Le projet sur gitlab.com',
                to: 'https://gitlab.com/kgaut/gitlab-ci-templates',
              },
              {
                label: 'Me contacter',
                to: 'https://kgaut.net/contact.html',
              },
            ],
          },
          {
            title: 'Wam sur les internet',
            items: [
              {
                label: 'Kgaut.NET',
                href: 'https://kgaut.net',
              },
              {
                label: 'Mastodon',
                href: 'https://oisaur.com/@kgaut',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/kgaut',
              },
            ],
          },
          {
            title: 'Et encore...',
            items: [
              {
                label: 'Wam sur Gitlab',
                href: 'https://gitlab.com/kgaut',
              },
              {
                label: 'Wam sur Github',
                href: 'https://github.com/kgaut',
              },
              {
                label: 'Wam sur Linkedin',
                href: 'https://www.linkedin.com/in/kevingautreau/',
              },
            ],
          },
        ],
        copyright: `Par Kevin Gautreau, Documentation construite avec Docusaurus.`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
      matomo: {
        matomoUrl: 'https://pw.kgaut.net/',
        siteId: '18',
        phpLoader: 'matomo.php',
        jsLoader: 'matomo.js',
      },
    }),
};
export default config;
